import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// localStorage
import { Storage } from '@ionic/storage';

declare var jquery: any;
declare var $: any;

// rxjs
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private _authenticated: boolean = false;
  public isAuthinticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello AuthProvider Provider');

    this.checkAuthenticated().then(res => { console.log(res)}) ;
  } 

  setAuthinticate(value: boolean) {
    this.isAuthinticated.next(value);
  }

  setUser(value: any) {
    this.user.next(value);
  }

  login(credential: any): Promise<any> {
    this._authenticated = false;
    console.log(credential);

    return new Promise((resolve, reject) => {

      $.ajax({
        url: 'https://offcloud.com/api/login/classic',
        data: credential,
        type: 'POST',
        crossDomain: true, // enable this
        /*xhrFields: {
          withCredentials: true
        },*/
        success: (data, status, xhr) => {
          console.log(data);
          this.getAccountInfoAng().subscribe(res => { console.log(res) });
          if (data.error) {
            console.log(data);
            this.setAuthinticate(false);
            resolve(data); 
          } else {

            this.setUser({
              username: data.email
            });

            this.storage.ready().then(() => {
              this.storage.set('username', data.email);
              this.storage.set('userId', data.userid);
            });

            console.log(xhr.getResponseHeader("connect.sid"));
            console.log(getCookie("connect.sid"));
            this.setAuthinticate(true);
            resolve(data);
          }

          
        },
        error: (err) => { console.log('Failed auth!', err); this.setAuthinticate(false); resolve(err);  }
      });

      function getCookie(cookieName) {
        var cookieArray = document.cookie.split(';');
        for (var i = 0; i < cookieArray.length; i++) {
          var cookie = cookieArray[i];
          while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1);
          }
          var cookieHalves = cookie.split('=');
          if (cookieHalves[0] == cookieName) {
            return cookieHalves[1];
          }
        }
        return "";
      }

    });
  }

  logout() {
    this.setAuthinticate(false);
    this.storage.ready().then(() => {
      this.storage.remove('username');
      this.storage.remove('userId');
    });
  }

  instandDownload(url: string) {
    $.ajax({
      url: 'https://offcloud.com/api/instant/download?apikey=KumpmHa0jnWMqhKDJg2lY6ZWiw4w6cT0',
      data: { 'url': url },
      type: 'POST',
      crossDomain: true, // enable this
      xhrFields: {
        withCredentials: true
      },
      success: function (data) { console.log(data); },
      error: function () { console.log('Failed!'); }
    });	
  }

  remoteDownload(url: string, remoteOptionId: string, folderId: string) {

    $.ajax({
      url: 'https://offcloud.com/api/instant/download?apikey=KumpmHa0jnWMqhKDJg2lY6ZWiw4w6cT0',
      data: { 'url': url, remoteOptionId: remoteOptionId, folderId: folderId },
      type: 'POST',
      crossDomain: true, // enable this
      xhrFields: {
        withCredentials: true
      },
      success: function (data) { console.log(data); },
      error: function () { console.log('Failed!'); }
    });	
  }

  checkAuthenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.ready().then(() => {
        this.storage.get('username').then((name) => {
          console.log('Me: Hey, ' + name + '! You have a very nice name.');
          
          if (name) {
            this.setAuthinticate(true);
            this.setUser({
              username: name
            });
            resolve(true);
          } else {
            this.setAuthinticate(false);
            resolve(false);
          }
        }).catch( err => reject(err));
      });
    })
  }

  getAccountInfo(): Promise<any> {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: 'https://offcloud.com/api/remote-account/list',
        type: 'GET',
        contentType: "text/plain",
        /*headers: {
          'Content-Type': 'text/plain',
          'Access-Control-Allow-Origin' : 'true'
        },*/
        xhrFields: { withCredentials: true },
        crossDomain: true, // enable this
        success: function (data) {
          console.log(data);
          resolve(data);
        },
        error: function (err) { console.log('Failed!', err); reject(err) }
      });
    });
  }

  getAPIKay() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),

      withCredentials: true,
      crossDomain: true,
     
    };

    return this.http.post('https://offcloud.com/api/proxy/list?apikey=KumpmHa0jnWMqhKDJg2lY6ZWiw4w6cT0', {}, httpOptions); 
  }

  getAccountInfoAng(): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      withCredentials: true,
      crossDomain: true,
      useValue: { withCredentials: true },
      xhrFields: { withCredentials: true },
    };

    return this.http.post('https://offcloud.com/api/remote-account/list?apikey=KumpmHa0jnWMqhKDJg2lY6ZWiw4w6cT0', httpOptions); 
  }

  getLinks() {


      $.ajax({
          url: 'https://offcloud.com/api/remote-account/list?apikey=ykQV8pJz8rn8dj6DnULMD2gwimdfpFyB',
          data: {},
          type: 'POST',
          headers: {
              'Content-Type': 'text/plain' 
          },

          crossDomain: true, // enable this
          xhrFields: {
              withCredentials: false
          },
          success: function(data) {
              console.log(data);


          },
          error: function(err) { console.log('Failed!', err); }
      });	


       
  }

}
