// core
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// libraries
import { MaterialModule } from './material/material.module';


@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        MaterialModule,
    ],
    declarations: [

    ],
    entryComponents: [],
    providers: [
    ],
    exports: [
        // modules
        HttpClientModule,
        MaterialModule,
    ]
})
export class SharedModule { }
