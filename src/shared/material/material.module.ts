// angular
import { NgModule } from '@angular/core';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import {
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatDividerModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatFormFieldModule, MatButtonModule, MatSlideToggleModule
} from '@angular/material';


@NgModule({
  exports: [
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
      MatSlideToggleModule,
      MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
    ]
})
export class MaterialModule {
}
