import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavParams, App, Nav, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// pages
//import { HistoryPage } from '../pages/history/history';

// providers
import { AuthProvider } from '../providers/auth/auth';

//import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
 // @ViewChild('myNav') nav: NavController
  rootPage: any;
  activeMenu: string;
  showSplash: boolean = false;
  pages: Array<{ title: string, component: any, icon: string }>;
  authenticated: boolean = true;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loadingCtrl: LoadingController,
    public authProvider: AuthProvider,
    public menu: MenuController
  ) {

    this.initializeApp();

    this.pages = [
      { title: 'Home', component: 'page-home', icon: 'home' },
      { title: 'Upload', component: 'page-upload', icon: 'cloud_upload' }, 
      { title: 'History', component: 'page-history', icon: 'history' }, 
      { title: 'Settings', component: 'page-settings', icon: 'settings' },
      { title: 'Account', component: 'page-account', icon: 'account_box' }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.showSplash = false;
      this.splashScreen.hide();
    });

    let loading = this.loadingCtrl.create();

    this.authProvider.isAuthinticated.subscribe((val: boolean) => {
      

      if (val === null) return;
      console.log(val);
      if (val) {
        this.menu.enable(true, 'authenticated');
        this.rootPage = 'page-home';
      } else {
        this.rootPage = 'page-login';
        this.menu.enable(false, 'authenticated');
      }
    });

    //this.nav.setRoot('page-history');
    //this.rootPage = 'HomePage'; 
  }

  

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.authProvider.logout();
    this.menu.enable(false, 'authenticated');
    this.nav.setRoot('page-login');
  }
}

