import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPage } from './settings';

import { CommonModule } from '@angular/common';

// modules
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    SettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPage),
    CommonModule,
    SharedModule
  ],
  exports: [
    SettingsPage
  ]
})
export class SettingsPageModule {}
