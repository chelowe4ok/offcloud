import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// dialog
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

// providers
import { AuthProvider } from './../../providers/auth/auth';

/**
 * Generated class for the UploadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'page-upload' })
@Component({
  selector: 'page-upload',
  templateUrl: 'upload.html',
})
export class UploadPage {

  url: string;
  currentRemote: string;
  remotes: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public dialog: MatDialog, private authProvier: AuthProvider) {

    this.authProvier.getAccountInfoAng().subscribe(res => { console.log(res); this.remotes = res.data; });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadPage');
  }

  openRemoteDialog(): void {
    let dialogRef = this.dialog.open(RemoteDownloadDialog, {
      width: '250px',
      data: { name: this.url, currentRemote: this.currentRemote, remotes: this.remotes }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if (result) this.remoteDownload();
    });
  }

  remoteDownload() {
    this.authProvier.remoteDownload(this.url, this.remotes[0].remoteOptionId, "/");
  }

}

@Component({
  selector: 'remote-dialog',
  templateUrl: 'dialog.html',
})
export class RemoteDownloadDialog {

  constructor(
    public dialogRef: MatDialogRef<RemoteDownloadDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
