import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadPage } from './upload';

import { RemoteDownloadDialog } from './upload'

import { CommonModule } from '@angular/common';

// modules
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    UploadPage,
    RemoteDownloadDialog
  ],
  entryComponents: [RemoteDownloadDialog],
  imports: [
    IonicPageModule.forChild(UploadPage),
    CommonModule,
    SharedModule
  ],
})
export class UploadPageModule {}
