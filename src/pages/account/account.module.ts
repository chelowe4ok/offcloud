import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import { CommonModule } from '@angular/common';

// modules
import { SharedModule } from './../../shared/shared.module';
@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountPage),
    CommonModule,
    SharedModule
  ],
})
export class AccountPageModule {}
