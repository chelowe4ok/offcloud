import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// providers
import { AuthProvider } from './../../providers/auth/auth';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage({ name: 'page-account' })
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  error: any;
  data: any;
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvier: AuthProvider) {
    this.authProvier.getAccountInfoAng().subscribe(res => { console.log(res); this.data = res.data; });

    this.authProvier.getAPIKay().subscribe(res => { console.log(res); });

    this.authProvier.user.subscribe(user => { this.user = user;})
    /*this.authProvier.getAccountInfo().then(data => {
      this.data = data;
    }).catch(err => this.error = err);*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

}
