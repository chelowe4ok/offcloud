import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryPage } from './history';

import { CommonModule } from '@angular/common';

// modules
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    HistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryPage),
    CommonModule,
    SharedModule
  ],
  exports: [  
    HistoryPage
  ]
})
export class HistoryPageModule {}
