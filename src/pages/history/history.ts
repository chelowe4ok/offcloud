import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// providers
import { AuthProvider } from './../../providers/auth/auth';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'page-history' })
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {


    public response: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private authProvier: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  ngOnInit() {

      /*fetch('https://offcloud.com/api/cloud/explore?apikeys=KumpmHa0jnWMqhKDJg2lY6ZWiw4w6cT0', {
          method: 'POST',
          headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
          body: {},
          credentials: 'include'
      }).then(response => console.log('SUCCESS', response));*/



      this.authProvier.getLinks();
  }

}
