import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// providers
import { AuthProvider } from './../../providers/auth/auth';

// models
import { User } from './../../models/user';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'page-login' })
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: User = new User();
  public errorMessage: string;
  res: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private authProvier: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage'); 
  }

  login(): void {
    console.log(this.user);
    this.authProvier.login({ username: this.user.email, password: this.user.password }).then(res => {
      console.log(res);

      if (res.error) {
        console.log(res.error);
        this.errorMessage = res.error;
        //this.navCtrl.setRoot('page-login');
      } else {
        console.log('home');
        this.navCtrl.setRoot('page-home');
      }
        
    }).catch(err => { console.log(err); });
    /*if (this.username == 'admin' && this.password == 'admin') {
      this.router.navigate(["user"]);
    } else {
      alert("Invalid credentials");
    }*/
  }
}
