import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { CommonModule } from '@angular/common';

// modules
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    CommonModule,
    SharedModule 
  ],
  exports: [LoginPage]
})
export class LoginPageModule {}
